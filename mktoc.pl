#!/usr/bin/env perl

# Read md-style title list from stdin, print HTML ToC to stdout
# TODO:
#	kindle-happy header and footer

use strict ;
use warnings;

sub makelink($$$) {
  my $file = shift;
  my $title = shift;
  my $level = shift;

  my $link = "<li><a href=\"" . $file;
  if ($level > 2) {
    # pandoc lowercases the text and replaces spaces with hyphens to generate the id
    my $anchor = lc($title);
    $anchor =~ s/\s+/-/g;
    $link .= "#$anchor";
  }
  $link .= '">' . $title . "</a>\n";
  return($link);
} # makelink()


# header
# note there's no ol4 because it's the default:  numeric
print "<html><head>\n" .
      '  <style type="text/css">' . "\n" .
      '    ol.ol2{list-style-type:upper-roman;}' . "\n" .
      '    ol.ol3{list-style-type:upper-alpha;}' . "\n" .
      '    ol.ol5{list-style-type:lower-alpha;}' . "\n" .
      '  </style></head><body>';

print "<ol class=\"ol2\">\n" ;

my $line ;
my $current_level = 2 ;
while ($line = <STDIN>) {
  # Count the hash marks to determine the level of the line to see if it differs
  $line =~ /^(.*)\.md:(#+) (.*)/ ;
  my $file = $1 ;
  my $level = length($2) ;
  my $title = $3 ;
  #print "len $level: " . $level . "\n" ;

  if ($level > $current_level) {
    $current_level = $level ;
    print "<ol class=\"ol$level\">\n" . makelink($file, $title, $level);
  }
  elsif ($level < $current_level) {
    $current_level = $level ;
    print makelink($file, $title, $level) . "</ol>\n";
  }
  else {
    print makelink($file, $title, $level);
  }

} # while ($line = <STDIN>)

print "</ol></html></body>\n" ;
