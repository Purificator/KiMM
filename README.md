# KiMM
<b>Ki</b>ndle   
<b>M</b>obi   
<b>M</b>arkdown

This converts a set of markdown (and image) files into a Kindle Mobi ebook.
It is useful for publishing your wiki documentation in other formats, especially
in a continuous integration process.

It uses Pandoc, so you have access to all of Pandoc's markdown extensions, like
footnotes.  Consult the documentation for Pandoc to find the full list and
syntax.

## Status
Overall status:  pre-beta

Each feature can be "done" (self-explanatory), "working" (works but
needs improvement), "not working" (does something like what it should, but
result needs manual fixing), "broken" (self-explanatory), "not started" 
(self-explanatory):

| **Feature**                | **Status**   | **Documentation** |
| -------------------------- | ----------   | -----------: |
| word count                 | done         |   documented |
| spell check                | working      |   documented |
| entity check               | experimental | undocumented |
| generate automatic ToC     | broken       | undocumented |
| generate ToC from manifest | done         | undocumented |
| generate HTML              | done         | little documented |
| generate kindle ebook      | working      | little documented |
| generate epub ebook        | experimental | undocumented |
| generate pdf               | experimental | little documented |
  

## Requirements
  * pandoc
  * perl
  * GNU make
  * Bourne shell (/bin/sh)
  * aspell (with your locale)
  * htmldoc (optional, for the pdf target)
  * gpg (if you want encryption)
  * KindleGen (to create Kindle .mobi files)


## Documentation
This project assumes all markdown files are in one directory containing all the
files from KiMM (except **README.md**, which you are free to delete or not).


### Best Practices
If you want the ebook to have conventional chapters rather than just sections,
it's better to put each chapter in its own markdown file.  Converting between
formats often loses chapter breaks with element tags, unfortunately.

Test hyperlinks aggressively.  The conversion process will produce different
results for an ebook built from many (body) files than it will with only one
file.  It tends to assume the latter even in the case of the former.  You
may have to give up on markdown for hyperlinks and use raw HTML.  For example,
a chapter called "hello.md", referred to from another chapter (e.g., 
"goodbye.md") in markdown, may wind up being converted to an anchor called
"hello" inside goodbye.html.  To work around this, you can use this HTML
instead:
'''HTML
	<a href="hello.html">Hello</a>
'''
Note that "hello.md" becomes "hello.html."  This is the case for all .md files
KiMM finds.




### Files

#### config.mk
This file contains the following configuration options:
  * _AUTHOR_ - The author's name in the form "LastName, FirstName"
  * _BOOK_ - The base file name for output, not affecting book contents at all
  * _COUNT_ - The minimum word count, "0" meaning to count without testing
  * _COVER_ - The cover image file name (jpeg preferred)
  * _ISBN_ - The book's ISBN
  * _SUBJECT_ - The book's subject (like "Fiction")
  * _TITLE_ - The book's full title as it would appear on the title page

#### dict
This is an aspell/ispell dictionary file.  Consult the man page for details,
but it's basically a list of words, one per line, to add to the existing
dictionary with a first line that looks like
```
personal_ws-1.1 en 0
```
The "en" should be your locale's language abbreviation.  Aspell maintains
the number at the end (and sorts the file with each run).  See the dict file
in this project for an example.

#### manifest
This is a list of file names, without the .md extension, in the order they
should appear.  Lines beginning with at "#" are skipped.  The first "chapter"
in the manifest will be used as the title page. Refer to the manifest in this
project for an example.

#### \*.skip
For any .md file you can skip portions of the testing using a skip file.  The
skip file is the name of the .md file with ".skip" appended (e.g., "foo.md"
would have a skip file called "foo.md.skip."  The skip file contains a list
of make targets, one per line, you wish to skip.  For example, a README.md.skip
containing "count" would skip the word count check.

#### style.css
If you want a style sheet you can include one with this name.  It is optional.



### Word Count
Inside **Makefile** is a variable _COUNT_ you can configure as a target word
count.  If your word count falls short **make** will throw an error.  Once
configured, you can run **make count** to get a word count for _all_ the
markdown (i.e., "\*.md") files in the current directory.  The output will
include the word count for each individual markdown file and a total.

```
$ make count
   253 Intro.md
   769 Bodtro.md
   710 Outro.md
  1732 total
Target word count 85000 not met
Makefile:38: recipe for target 'count' failed
make: *** [count] Error 1
```


### Spell Check
Spell check requires **aspell** (or **ispell** with a symbolic link for
**aspell**).  Running **make test** will check the spelling for each file
individually and produce a behemoth report of all the errors it finds, by file.
It currently supports a global override dictionary file for words not in your
locale's dictionary, called **dict** (in the same directory as everything else,
of course).  The dictionary file is in the aspell/ispell dictionary format,
which is documented in their respective man pages.

If **make test** encounters any spelling errors then it will exit with an
error, otherwise it will exit cleanly with the only output being
"Testing for spelling errors"


### Building
Running "**make**" will build everything and run all checks, including 
"**count**" and "**test**."  These are the most useful individual
targets for **make**:
   * **count** - Perform a word count.
   * **html** - Create single HTML file.  Remember to copy all images, as well.
   * **mobi** - Create a mobi file.
   * **pdf** - Create a PDF file.
   * **test** - Run tests, currently only spell check.
