#!/bin/sh

# Install 

FILES="	\
	Makefile \
	manifest2toc.pl \
	manifest_missing.pl \
	mkopf.sh \
	mktoc.pl \
	test.sh \
	count.sh \
	entities.pl \
	sample_style.css \
"

[ ! "$1" ] && echo "Usage:  $9 <target-directory>" && exit 1
chmod +x *.sh *.pl

cp -p $FILES $1
[ ! -f $1/config.mk ] && cp config.mk $1

echo "You may wish to add the these to your revison control's ignore list:"
for FILE in $FILES; do
    echo $FILE
done

echo ""
echo "DONE"
exit 0
