#!/usr/bin/env perl

use strict;
use warnings;

if (! open(MANIFEST, "<manifest")) {
  die("Could not open manifest: $!\n");
}

my %chapters;
my $line;
my $rv = 0;
while ($line = <MANIFEST>) {
  if ($line =~ /^#/) {  # skip comments
    next;
  }
  chomp($line);

  $chapters{$line} = 1;
} # while $line = <MANIFEST>
close(MANIFEST);

my $dir;
if (! opendir($dir, ".")) {
  die("Could not read current directory: $!\n");
}

while ($line = readdir($dir)) {
  if ($line !~ /(.*)\.md$/) {
    next;
  }
  if (($1 eq "dontread") || ($1 eq "README")) {
    next;
  }

  if (! $chapters{$1}) {
    print $1 . " missing from manifest.\n";
    $rv++;
  }
} # while $line = readdir $dir

if ($rv > 0) {
  print "***** " . $rv . " chapters missing from manifest.\n";
}
exit $rv;
