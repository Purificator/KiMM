#
# Makefile
#

include config.mk


HT = pandoc --from markdown --to html --standalone
EP = pandoc --from markdown --epub-stylesheet style.css --epub-cover-image $(COVER) --epub-metadata html/$(BOOK).opf
MHT = $(shell grep '^[a-zA-Z0-9]' manifest | sed 's/\(.*\)/\1.html/g' | xargs)
MMD = $(shell grep '^[a-zA-Z0-9]' manifest | sed 's/\(.*\)/\1.md/g' | xargs)


all:	clean count test build
	@true

build:	opf pdf mobi
	@true

mobi:	chapters html opf
	cd html && \
	kindlegen -c2 -verbose $(BOOK).opf -o $(BOOK).mobi
	mv html/$(BOOK).mobi .

epub:	opf
	$(EP) --to epub --output $(BOOK).epub $(MMD)
	$(EP) --to epub3 --output $(BOOK).epub3 $(MMD)

titles:
	grep '^#' *.md > titles.list

testtoc:	titles
	@mkdir -p html
	cat titles.list | perl mktoc.pl > html/toc.html

toc:
	@mkdir -p html
	perl manifest2toc.pl "$(TITLE)" "$(AUTHOR)" "$(ISBN)"

opf:	html
	cd html && \
	sh ../mkopf.sh $(BOOK) "$(AUTHOR)" "$(ISBN)" "$(TITLE)" $(SUBJECT) $(COVER) $(MHT)

chapters:	$(patsubst %.md,%.html,$(wildcard *.md)) Makefile
	@mkdir -p html
	@mv $(MHT) html
	@touch __dum.jpg __dum.gif __dum.png __dum.bmp __dum.svg __dum.css
	@cp *.jpg *.gif *.png *.bmp *.svg *.css html
	@rm -f __dum.jpg __dum.gif __dum.png __dum.bmp __dum.svg __dum.css

pdf:	chapters
	cd html && \
	htmldoc --webpage --titleimage $(COVER) -f ../$(BOOK).pdf toc.html $(MHT)
	@echo Created $(BOOK).pdf

# If you use GIMP and save the xcf source images you'd be smart to compress
# them before adding them to your repo.  TODO:  make confiurable
source-images:
	cd source_images ; \
	bunzip2 -k -f *.xcf.bz2

manifest-check:
check-manifest:
	perl manifest_missing.pl

html:	toc
	@mkdir -p html
	cp style.css css.tmp || touch css.tmp
	$(HT) --toc --self-contained --css=css.tmp $(MMD) | \
		sed 's/<h2/<div class="pagebreak"><\/div><h2/g' \
			> $(BOOK).html
	rm -f css.tmp

count:
	@sh count.sh $(COUNT)

encrypt:
	@echo "This sucks, but it can't go on gitlab in clear text."
	gpg -c -a profile

decrypt:
	@echo This sucks, but privacy and security matter.  
	@echo Use "profile" for the clear text output file.
	gpg -d profile.asc


entities:	check-entities

# This is still in development, so don't expect much if you use it.
check-entities:
	@perl entities.pl $(MMD) 

test:
	@sh test.sh

clean:
	rm -rf html build.list titles.list $(BOOK).mobi $(BOOK).pdf __*.* \
		$(BOOK).iml $(BOOK).html *.tmp README.html

%.html: %.md
	$(HT) --css=style.css $< --output $@

#help:
#	@echo ""
#	@grep '^[a-z]*:' Makefile | sed 's/:.*//g'
#	@echo ""
