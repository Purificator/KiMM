################################################################################
################################################################################
## Make Configuration

# Author's name, in the form "Last, First"
AUTHOR = Me

# A one-word name for the book, just used for output file names
BOOK = mybook

# Target word count, which can be "0" if you don't wish to track
COUNT = 5000

# The name of the cover image (with only alphanumeric characters)
COVER = cover.jpg

# The book's ISBN code (if applicable, not necessary for Kindle books)
ISBN =

# The book's subject (e.g., "Fiction" or "Reference")
# See http://bisg.org/page/BISACSubjectCodes
SUBJECT = Fiction

# The full title
TITLE = My Book
################################################################################
################################################################################
