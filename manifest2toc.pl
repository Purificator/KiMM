#!/usr/bin/env perl
#
# make two HTML tables of contents from the manifest:
#	1) standalone ToC
#	2) insertable/includable ToC
# usage:  $0 <title> <author>
#

use strict;
use warnings;

if (@ARGV < 3) {
  print STDERR "You shouldn't run this by itself unless you know what you're " .
               "doing and it seems you don't.  Nonetheless:\n" .
               "Usage:  $0 <title> <author> <ISBN>\n"
}

my $manifest = 'manifest';
my $toc = 'toc.html';
my $ncx = 'toc.ncx';
my $title = $ARGV[0];
my $author = $ARGV[1];
my $isbn = $ARGV[2];
my $rv = 0;


# Make a pandoc-like anchor tag out of a human-readable title
sub makeanchor($) {
  my $title = shift;
  my $anchor = lc($title);
  # pandoc lowercases text and replaces spaces with hyphens to generate the id
  $anchor =~ s/([^\s\w\-\@?^=%&amp;~\/+\#\.]|\?|\s+$)//g;
  $anchor =~ s/\s+/-/g;
  return($anchor);
} # makeanchor()


# Create a chapter or section HTML link out of a title and/or file name
sub makelink($$$) {
  my $title = shift;
  my $file = shift;
  my $level = shift;
  my $link = "";

  # Spaces are superfluous but help debugging.  Remove this if you wish.
  my $i = $level ;
  while ($i-- > 0) {
    $link .= "   ";
  }

  if ($level > 2) {
    $link .= '<li><a href="' . $file . '.html#' . makeanchor($title) . '">' .
              $title . "</a></li>\n";
  }
  else {
    $link .= '<li><a href="' . $file . '.html">' . $title . "</a></li>\n";
  }
  return($link);
} # makelink()


#
# main
#
if (! open(IFP, "<$manifest")) {
  die("Failed to open $manifest for reading: $?\n");
}

if (! open(HTML, ">html/$toc")) {
  die("Failed to open $toc for writing: $?\n");
}

if (! open(NCX, ">html/$ncx")) {
  die("Failed to open $ncx for writing: $?\n");
}

# HTML header
my $css = "";
if ( -f "style.css") {
  $css = '<link rel="stylesheet" href="style.css">' . "\n";
}

# No ol4 because it's the default:  numeric
# Headings to h6 because that's as far as markdown goes (as documented, anyway)
print HTML "<!doctype html><head>\n" .
           '<meta http-equiv="content-type" content="text/html; ' . 
           'charset=UTF-8">' .  "\n$css\n" .
           '<style type="text/css">' . "\n" .
           'ol.ol2{list-style-type:upper-roman;}' . "\n" .
           'ol.ol3{list-style-type:upper-alpha;}' . "\n" .
           'ol.ol5{list-style-type:lower-alpha;}' . "\n" .
           'h1,h2,h3,h4,h5,h6{text-align:center;}' . "\n" .
           '</style>' . "\n";

# NCX header
print NCX '<?xml version="1.0" encoding="UTF-8"?>' . "\n" .
          '<!DOCTYPE ncx PUBLIC "-//NISO//DTD ncx 2005-1//EN"' . "\n" .
          '"http://www.daisy.org/z3986/2005/ncx-2005-1.dtd">' . "\n" .

          '<ncx version="2005-1" xml:lang="en" ' .
          'xmlns="http://www.daisy.org/z3986/2005/ncx/">' . "\n\n" .
          '<head>' . "\n" .
          '<meta name="dtb:depth" content="1"/> <!-- 1 or higher -->' . "\n" .
          '<meta name="dtb:totalPageCount" content="0"/>' . "\n" .
          '<meta name="dtb:maxPageNumber" content="0"/>>' . "\n";
if (length($isbn) > 0) {
  print NCX '<meta name="dtb:uid" content="' . $isbn . '"/>' . "\n";
}
print NCX '</head>' . "\n\n" .
          '<docTitle>' . "\n" .
          '  <text>' . $title . '</text>' . "\n" .
          '</docTitle>' . "\n" .
          '<docAuthor>' . "\n" .
          '  <text>' . $author . '</text>' . "\n" .
          '</docAuthor>' . "\n\n" .
          '<navMap>' . "\n" .
          '  <navPoint class="toc" id="tc" playOrder="1">' . "\n" .
          '    <navLabel><text>Table of Contents</text></navLabel>' . "\n" .
          '    <content src="' . $toc . '" /> ' . "\n" .
          '  </navPoint>' . "\n";







# ...
print HTML "</head><body><h2>Table of Contents</h2>\n";
# ...

# The meat:  build out an ordered list based on the manifest
print HTML '<div id="toc">' . "\n" . '<div><ol class="ol2">' . "\n";
my $line;
my $line_num = 1;
while ($line = <IFP>) {
  my ($mdline, $file, $title);

  chomp($line);

  # Skip comments and blanks; only take text lines
  if ($line =~ /^([\s#]|$)/) {
  next;
  }
  print "$line\n";
  $file = $line;

  # Print the chapter line then read the titles list file for subsections
  my $mdfile = "$file.md";
  if (! open(MD, "<$mdfile")) {
    print STDERR "failed to open $line.md for reading: $?\n";
    $rv++;
    next;
  }
  my $current_level = 2;
  my $level = 0;
  # The chapter title is the first line.  period.
  $mdline = <MD>;
  #print $mdline;
  if ($mdline =~  /^## (.*)/) {  # Chapter titles have two leading hash marks
    $title = $1;
    print HTML makelink($title, $file, 2);
  }
  else {
    if ($line_num > 1) {
      print STDERR "$mdfile does not have a chapter title on the first line\n";
      $rv++;
    }
    next;
  }

  # Print the NCX entry
  if ($line_num == 1) {
    print NCX '  <navPoint class="titlepage" id="tp" playOrder="2">' . "\n" .
              '    <navLabel><text>Title Page</text></navLabel>' . "\n" .
              '    <content src="' . $line . '.html" />' . "\n" .
              '  </navPoint>' . "\n";
  }
  else {
    print NCX '  <navPoint class="book" id="c' . ($line_num - 1) .
              ' playOrder="' .  ($line_num + 1) . '">' . "\n" .
              '    <navLabel><text>' . $title . '</text></navLabel>' . "\n" .
              '    <content src="' . $line . '.html" />' . "\n" .
              '  </navPoint>' . "\n";
  }
  $line_num++;

  while ($mdline = <MD>) {
    #print $mdline;
    if ($mdline !~ /^(#+)\s*(.*)/) {
    next;
    }
    #print $mdline;
    $level = length($1);
    $title = $2;
    #print "$title : $level\n";

    my $link;
    if ($level > $current_level) {
      $current_level = $level;
      print HTML "<div><ol class=\"ol$level\">\n" .
                 makelink($title, $file, $level);
    }
    elsif ($level < $current_level) {
      $current_level = $level;
      print HTML "</ol></div>\n" . makelink($title, $file, $level);
    }
    else {
      print HTML makelink($title, $file, $level);
    }
  } # while $mdline = MD

  if ($level > 2) {
    print HTML "</ol></div>\n";
  }
  close(MD);
} # while $line = IFP


print HTML "</ol></div>\n";
print NCX "</navMap>\n";

# Footer
print HTML "</div></body></html>\n";
print NCX "</ncx>\n";
close(HTML);
close(NCX);
close(IFP);

if ($rv > 0) {
  print STDERR "$rv errors\n";
}
exit($rv);
