#!/bin/sh

# "Unit" tests for the files.  Curently only spell check.
# TODO: produce junit-compatable XML results

echo "Testing for spelling errors"
TMP=/tmp/wtest.$$
RV=0
OUT=/tmp/spell.$$
for CHAPTER in *.md ; do
  # bypass files with a skip file for the test target
  grep -q '^test$' ${CHAPTER}.skip > /dev/null 2>&1 && continue

  # spelling (minus html tags).  We MUST check the return code for ispell.
  cat $CHAPTER | perl -pe 's/<.*?>//g; s/[-_*]/ /g;' | \
	aspell --mode=html -p ./dict -a >/dev/stdout 2>&1 > $TMP
	[ $? -ne 0 ] && RV=1
  grep -a '^&' $TMP > $OUT
  if [ -s "$OUT" ] ; then
    #let RV=$RV+1
    RV=`expr $RV + 1`
    printf "\n	++++++++++++++++++++++++ $CHAPTER\n"
    cat $OUT
  fi
done

rm -f $OUT $TMP
#echo $OUT $TMP
[ $RV -eq 0 ] && echo OK
exit $RV
