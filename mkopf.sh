#!/bin/sh

# Generate an OPF file from the manifest

[ $# -lt 7 ] && u_moron "Not enough arguments."

NAME=$1
[ ! "$NAME" ] && NAME=mybook
shift

AUTHOR=$1
shift
ISBN=$1
shift

TITLE="$1"
shift
SUBJECT="$1"
shift
COVER="$1"
shift
[ ! "$*" ] && u_moron "No files listed."

u_moron() {
  echo "$*"
  echo "Usage:  $0 <name> <author> <ISBN> <title> <subject> <cover> <file1> [<file2>] [ . . . ]"
  exit 1
}

AUTHOR2=`echo "$AUTHOR" | sed 's/\(.*\), \(.*\)/\2 \1/'`


DATE=`date "+%Y-%m-%d"`

TITLE_FILE=""
TITLE_TEXT=""


cat > $NAME.opf <<EOT
<?xml version="1.0"?>
<package version="2.0" xmlns="http://www.idpf.org/2007/opf" unique-identifier="BookId">
 
  <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
    <dc:title>$TITLE</dc:title>
    <dc:language>en-us</dc:language>
    <dc:creator opf:file-as="$AUTHOR" opf:role="aut">$AUTHOR2</dc:creator>
    <dc:publisher>Amazon.com</dc:publisher>
    <dc:subject>$SUBJECT</dc:subject>
    <dc:date>$DATE</dc:date>
    <dc:description>A possibly humorous book about nothing. </dc:description>
    <meta name="cover" content="cover-image" />
EOT
  if ["$ISBN" ] ; then
    echo "<dc:identifier id=\"BookId\" opf:scheme=\"ISBN\">$ISBN</dc:identifier>"
fi
cat >> $NAME.opf <<EOT
  </metadata>

  <manifest>
    <item id="tc" href="toc.html" media-type="application/xhtml+xml"/>
    <item id="ncx" href="toc.ncx" media-type="application/x-dtbncx+xml"/>  
    <item id="cover-image" href="$COVER" media-type="image/jpeg" />
EOT
if [ -f style.css ] ; then
    echo '<item id="stylesheet" href="style.css" media-type="text/css"/>' \
         >> $NAME.opf
fi
I=1
for FILE in $@ ; do
  if [ $I = 1 ] ; then
    TITLE_FILE=$FILE
    MD=`echo $FILE | sed 's/\.html/.md/g'`
    TITLE_TEXT=`grep '^#' ../$MD | head -1 | sed 's/^#*//' | xargs -0 echo`
  fi
  echo "    <item id=\"c$I\" href=\"$FILE\" media-type=\"application/xhtml+xml\" fallback-style=\"stylesheet\" />" >> $NAME.opf
  I=`expr $I + 1`
done

#
# Spine and Guide, describing the contents and order
#
cat >> $NAME.opf <<EOT
  </manifest>

  <spine toc="ncx">
    <itemref idref="tc" />
EOT
I=1
for FILE in $@ ; do
  echo "    <itemref idref=\"c$I\" />" >> $NAME.opf
  I=`expr $I + 1`
done

cat >> $NAME.opf <<EOT
  </spine>

  <guide>
    <reference type="toc" title="Table of Contents" href="toc.html"/>
    <reference type="text" title="$TITLE_TEXT" href="$TITLE_FILE"/>
EOT
#for FILE in $@ ; do
#  MD=`echo $FILE | sed 's/\.html/.md/g'`
#  # N.B. the glob below here has a space and a tab
#  TITLE=`grep '^#' ../$MD | head -1 | sed 's/^#*//' | xargs -0 echo`
#  echo "    <reference type=\"text\" title=\"$TITLE\"href=\"$FILE\"></reference>" >> $NAME.opf
#done
echo '  </guide>'
echo '</package>'
