#!/usr/bin/perl

# Find HTML entities and warn about them because many do not work with Kindle

use strict;
use warnings;

if (@ARGV < 1) {
  die("Usage: $0 <file> [ <file [ . . . ] ]\n");
}

# These are HTML entites documented or known to work on Kindle
my %good_entities = (
  "_x" => 1,
  "aacute" => 1,
  "agrave" => 1,
  "aelig", =>1,
  "eacute" => 1,
  "egrave" => 1,
  "iacute" => 1,
  "igrave" => 1,
  "nbsp" => 1,
  "oacute" => 1,
  "ograve" => 1,
  "thorn", =>1,
  "trade" => 1,
  "uacute" => 1,
  "ugrave" => 1,
);

my $file;
my $suspect_entities = 0;
foreach $file (@ARGV) {
  my $line;
  my $line_num = 1;
  my $printed_header = 0;
  my $printing_line;

  if (! open(FILE, "<$file")) {
    die("Could not open $file: $?\n");
  }

  # Check each line`
  while ($line = <FILE>) {
    my $word;
    my $entity;

    $printing_line = 0;
    foreach $word (split(/\s+/, $line)) {
      # TODO:  handle back-to-back entities like "&copy;&nbsp;"
      if ($word !~ /.*&(\w+);.*/) {
        next;
      }
      $entity = lc($1);
      if (exists($good_entities{lc($1)})) {
        #print STDERR "$entity is good in $word\n";
        next;
      }
      #print STDERR "$entity is not good in $word\n";

      if (!$printed_header) {
        print "$file has suspect HTML entities:\n";
        $printed_header = 1;
      }
      if (!$printing_line) {
        print '  ' . $line_num . ' ' . $line;
        $printing_line = 1;
      }
      print '    &' . $1 . ";\n";
      $suspect_entities++;
    } # foreach my $word (split(/\s+/, $line))

    $line_num++;
  }
} # foreach $file (@ARGV)


# Only warn, never error.  This is to assist debugging; ultimate responsibility
# for checking entities is with the user.
print "$suspect_entities susppect entities found\n";
exit 0;
