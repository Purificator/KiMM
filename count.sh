#!/bin/sh

# Generate a word count

TARGET=$1
[ ! "$TARGET" ] && echo "Usage:  $0 <target-count>"
TMP="count$$.tmp"

# Get a list of files to count
for FILE in *.md ; do
  grep -q '^count$' $FILE.skip > /dev/null 2>&1 || echo $FILE >> $TMP
done


# Total count
COUNT=`cat $TMP | xargs wc -w | tail -1 | awk '{print $1}'`

# Per-file count
cat $TMP | xargs wc -w

rm -f $TMP
[ $COUNT -lt $TARGET ] && echo "Target word count $TARGET not met" && exit 1
echo "Word count OK"
exit 0

